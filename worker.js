var fs = require('fs');
var express = require('express');
var serveStatic = require('serve-static');
var path = require('path');
var express = require('express');
var morgan = require('morgan');
var AWS = require('aws-sdk');
var healthChecker = require('sc-framework-health-check');
const uuidv4 = require('uuid/v4');
var FB = require('fb');

var fb = new FB.Facebook({
    version: 'v2.8'
        // client_id: 'd885e87c64ed19d27098e821336c25d4',
        // appId: '1254109294657801',
        // appSecret: 'd8cbcf939074060d5367fb57773b54cd'
});

var ENV = process.env.NODE_ENV || 'development';
var config = "";

console.log("ENV for Identity poller = " + ENV);

try {
    if (ENV === "development") {
        console.log("Server deployment type: DEV");
        config = require("./urlconfig.dev.json");
    } else if (ENV === "production") {
        console.log("Server deployment type: PROD");
        config = require("./urlconfig.prod.json");
    } else if (ENV === "testing") {
        console.log("Server deployment type: TEST");
        config = require("./urlconfig.test.json");
    } else if (ENV === "alpha") {
        console.log("Server deployment type: ALPHA");
        config = require("./urlconfig.alpha.json");
    } else if (ENV === "alpha_new") {
        console.log("Server deployment type: ALPHA");
        config = require("./urlconfig.alpha_new.json");
    } else {
        console.log("Server deployment type NOT detected");
        process.exit(1);
    }
} catch (e) {
    console.error("Couldn't initialize the script due to broken configuration.\nPlease check the config.json\n" + e);
    process.exit(1);
}

var pageId = config.FB_PAGE.pageId;
var pageToken = config.FB_PAGE.token;

console.log('pageId:%s && pageToken:%s', pageId, pageToken);

AWS.config.update({
    region: config.AWS_CONFIG.region,
    accessKeyId: config.AWS_CONFIG.accessKeyId,
    secretAccessKey: config.AWS_CONFIG.secretAccessKey
});

function isJSONString(jsonString) {
    try {
        var o = JSON.parse(jsonString);
        if (o && typeof o === "object") {
            return o;
        }
    } catch (e) {}

    return false;
};

module.exports.run = function(worker) {

    console.log('   >> Worker PID:', process.pid);
    var environment = worker.options.environment;

    var app = express();

    var httpServer = worker.httpServer;
    var scServer = worker.scServer;

    if (environment == 'dev') {
        // Log every HTTP request. See https://github.com/expressjs/morgan for other
        // available formats.
        app.use(morgan('dev'));
    }
    app.use(serveStatic(path.resolve(__dirname, 'public')));

    // Add GET /health-check express route
    healthChecker.attach(worker, app);

    httpServer.on('request', app);

    var count = 0;

    /*
      In here we handle our incoming realtime connections and listen for events.
    */
    scServer.on('connection', function(socket) {

        console.log("Connection: " + socket.id);

        socket.exchange.publish('welcome', "hi there!!", function(err) {
            if (err) {
                console.log("cant welcome" + err);
            } else {
                console.log("Greeted!");
            }
        });

        // Some sample logic to show how to handle client events,
        // replace this with your own logic

        socket.on('sampleClientEvent', function(data) {
            count++;
            console.log('Handled sampleClientEvent', data);
            scServer.exchange.publish('sample', count);
        });

        socket.on('message', function(data) {
            console.log("raw message: " + data);
            console.log('type of raw msg before parse:', typeof(data));

            if (!isJSONString(data)) {
                console.log("raw data is not json");
                return false;
            }

            var parsedJSON = JSON.parse(data);

            console.log('type of raw msg after parse:', typeof(parsedJSON));
            var event = parsedJSON.event;
            var channel = parsedJSON.data.channel;
            console.log('channel:%s', channel);

            if (isJSONString(data) && event == '#publish') {

                var incoming = parsedJSON.data;

                console.log('typeof of incoming:', typeof(incoming));

                let isAxmLog = false;

                if (typeof(incoming.data.axmId) !== 'undefined' && typeof(incoming.data.isTokenIssued) !== 'undefined') {
                    isAxmLog = true;
                }

                console.log(`isAXMLOg:${isAxmLog}`);

                if (isJSONString(JSON.stringify(incoming.data)) && !isAxmLog) {

                    console.log("message for channel: " + JSON.stringify(incoming.data));
                    //postToFb(incoming.data);
                    const postType = incoming.data.post_type;
                    const postContent = incoming.data.post_content;
                    var msg = postContent.msg_content;
                    var sender_name = postContent.sender_name;

                    //FOR SHARING POST 

                    var bData = {
                        post_id: postContent.post_id || '',
                        msg_id: postContent.chat_id,
                        channel_id: postContent.channel_id,
                        channel_name: postContent.channel_name,
                        msg: msg,
                        sender_name: postContent.sender_name,
                        sender_id: postContent.sender_id,
                        sender_avatar: postContent.sender_avatar,
                        msg_type: postContent.message_type,
                        msg_media_url: postContent.msg_media_url,
                        story: postContent.story,
                        receiver_id: postContent.receiver_id,
                        receiver_type: postContent.receiver_type,
                        receiver_name: postContent.receiver_name,
                        msg_sha: postContent.msg_sha,
                        msg_created_at: postContent.created_at,
                        msg_modified_at: postContent.modified_at,
                        msg_recieved_at: postContent.received_at,
                        is_msg_delivered: postContent.is_delivered,
                        s_f_user_id: postContent.s_f_user_id || '', //sharing from user id
                        s_f_channel_id: postContent.s_f_channel_id || '', //sharing from channel id
                        s_og_post_id: postContent.s_og_post_id || '', //original shared post id
                    }

                    const postPayload = {
                        post_type: postType,
                        post_content: bData
                    }

                    //since every chat msg is published on general channel
                    //so to avoid chat recived on general channel from re-publishing on general channel 
                    // if (channel !== 'general') {
                    //     scServer.global.publish('general', postPayload);
                    // }

                    var sqs = new AWS.SQS({
                        region: config.AWS_CONFIG.region
                    });

                    var sqsParams = {
                        MessageBody: JSON.stringify(postPayload),
                        QueueUrl: config.AWS_SQS_COMMUNITY.queueUrl
                    };

                    sqs.sendMessage(sqsParams, function(err, data) {
                        if (err) {
                            console.log('ERR', err);
                        } else {
                            console.log(data);
                        }
                    });


                } else {
                    console.log("data from RAW is NOT json");
                    console.log("data from raw: " + JSON.stringify(incoming.data));
                }


            } else {
                console.log("raw message NOT json");
            }
        });

        // var interval = setInterval(function() {
        //     socket.emit('random', {
        //         rand: Math.floor(Date.now() / 1000) + " dummy"
        //     });
        // }, 10000);

        socket.on('disconnect', function() {
            // clearInterval(interval);
            console.log('Client disconnected.');
        });

        socket.on('disconnect', function() {
            // clearInterval(interval);
            console.log('Client disconnected.');
        });

        socket.on('unsubscribe', function() {
            console.log('Client unsubscribed.');
        });

        socket.on('subscribe', function() {
            console.log('Client subscribed.');
            console.log(socket.subscriptions());
        });

    });

    var myGeneralChannel = scServer.global.subscribe('general');

    myGeneralChannel.watch(function(data) {
        console.log("general collector: " + data);
    });

    function postToFb(msgJSON) {
        FB.setAccessToken(pageToken);
        getPageToken(function() {
            if (msgJSON.msg_media_url.length) {
                //image post
                var body = {
                    url: msgJSON.msg_media_url,
                    caption: msgJSON.msg_content.length ? msgJSON.msg_content : 'photos from verifyme.'
                };

                FB.api(pageId + '/photos', 'post', body, function(res) {
                    if (!res || res.error) {
                        console.log(!res ? 'error occurred' : res.error);
                        return;
                    }
                    console.log('Post Id: ' + res.id);
                });

            } else {
                //text post
                var body = msgJSON.msg_content;
                FB.api(pageId + '/feed', 'post', { message: body }, function(res) {
                    if (!res || res.error) {
                        console.log(!res ? 'error occurred' : res.error);
                        return;
                    }
                    console.log('Post Id: ' + res.id);
                });
            }
        })
    }

    function getPageToken(callback) {
        console.log('generating page token for pageId:%s', pageId);
        // axios.get(`https://graph.facebook.com/${pageId}?fields=access_token`)
        // .then(function(response){
        //   console.log('page token--');
        //   console.log(response);
        // })
        // .catch(function(error){
        //   console.log(error);
        // });
        var url = pageId + '?fields=access_token';
        FB.api(url, 'get', function(response) {
            if (!response || response.error) {
                console.log(!response ? 'error occurred' : response.error);
                return;
            }
            console.log(response);
            console.log('access_token: ' + response.access_token);
            FB.setAccessToken(response.access_token);
            callback();
        });
    }
};