#!/bin/bash
echo "Testing Remote Execution" > /home/ubuntu/remote_test.txt
cd /home/ubuntu/verifyme_socket
git remote update
git pull
npm install
pm2 reload SOCKET
echo "Done"