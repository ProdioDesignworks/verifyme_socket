const path = require('path');
const fs = require('fs');

const privateKeyPath = path.join(__dirname, 'SSL', 'star_verifyme_in.key');
const certificatePath = path.join(__dirname, 'SSL', 'star_verifyme_in.crt');

// exports.privateKey = fs.readFileSync(privateKeyPath).toString();
// exports.certificate = fs.readFileSync(certificatePath).toString();

module.exports = {
	key: fs.readFileSync(privateKeyPath).toString(),
  	cert: fs.readFileSync(certificatePath).toString()
};